<?php

namespace App\Jobs;

use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\AuthService;
use App\Services\Contracts\ImageApiService;
use App\Values\Image;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $image;
    private $filter;
    private $user;


    public function __construct($image, $filter)
    {
        $this->image = $image;
        $this->filter = $filter;

    }


    public function handle(ImageApiService $imageApiService, AuthService $authService)
    {
        $result = $imageApiService->applyFilter($this->image->getSrc(), $this->filter);

        $newImage = new Image(
            $this->image->getId(),
            $result,
        );

        $authService->getUser()->notify(new ImageProcessedNotification($newImage));
    }

    public function failed(Exception $exception)
    {
        request()->user()->notify(new ImageProcessingFailedNotification($this->image, $this->filter, $exception));
    }
}
