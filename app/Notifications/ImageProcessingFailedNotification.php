<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class ImageProcessingFailedNotification extends Notification
{
    use Queueable;

    private $image;
    private $filter;

    private $exception;

    public function __construct($image, $filter, $exception)
    {
        $this->image = $image;
        $this->filter = $filter;
        $this->exception = $exception;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url($this->image->getSrc());

        return (new MailMessage)
                    ->mailer('log')
                    ->line('Dear ' . request()->user()->name . ',')
                    ->line('The applying the filter "' . $this->filter . '" was failed to the image:
')
                    ->line(new HtmlString('<a href="'.$this->image->getSrc().'"><img src="'.$this->image->getSrc().'" alt="Image"></a>'))
                    ->line('Best regards, ')
                    ->line('Binary Studio Academy');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'status' => 'failed',
            'message' => $this->exception->getMessage(),
            'image' => [
                'id' => $this->image->getId(),
                'src' => $this->image->getSrc()
            ]
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage($this->toArray($notifiable));
    }
}
