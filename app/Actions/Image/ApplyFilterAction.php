<?php

declare(strict_types=1);

namespace App\Actions\Image;

use App\Jobs\ImageJob;
use App\Services\Contracts\ImageApiService;
use App\Actions\Contracts\Response;
use App\Values\Image;

class ApplyFilterAction
{

	public function __construct()
	{

	}

	public function execute(Image $image, string $filter)
	{
        ImageJob::dispatch($image, $filter);
	}
}
